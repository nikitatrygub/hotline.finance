export async function getTasks() {
  let tasksReq = await fetch("https://jsonplaceholder.typicode.com/todos");
  let tasks = await tasksReq.json();
  return tasks;
}
