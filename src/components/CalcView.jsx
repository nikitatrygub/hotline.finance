import React from "react";
import Slider, { createSliderWithTooltip } from "rc-slider";
import "rc-slider/assets/index.css";
import Select from "react-select";

const SliderWithToolTip = createSliderWithTooltip(Slider);

const optionsCurr = [
  { value: "usd", label: "usd" },
  { value: "uah", label: "uah" }
];

const CalcView = ({
  state,
  setCurrCurency,
  setCurrentCreditSummAfterAction,
  setCurrentCreditSummAction,
  calcData,
  setDeliveryTime
}) => {
  const {
    allDeliveryTimes,
    currentCurrency,
    currentMinCreditSumm,
    currentMaxCreditSumm,
    currentCreditSumm,
    currentDeliveryTime
  } = state;

  const handleSliderChange = value => {
    setCurrentCreditSummAction(value);
  };

  const handleSliderAfterChange = value => {
    setCurrentCreditSummAfterAction(value);
  };

  const handleCurrCurency = selectedOption => {
    setCurrCurency(selectedOption);
  };

  const handleDeliveryTime = selectedOption => {
    setDeliveryTime(selectedOption);
  };

  return (
    <div className="calc">
      <div className="calc__title">Расчет по кредиту:</div>
      <div className="calc__summ">
        Сумма:
        <SliderWithToolTip
          value={currentCreditSumm}
          onChange={handleSliderChange}
          onAfterChange={handleSliderAfterChange}
          min={currentMinCreditSumm}
          max={currentMaxCreditSumm}
        />
      </div>
      <div className="calc__currency">
        Валюта:
        <Select
          className="select"
          value={currentCurrency}
          options={optionsCurr}
          onChange={handleCurrCurency}
        />
      </div>
      <div className="calc__currency">
        Срок:
        <Select
          className="select"
          value={currentDeliveryTime}
          options={allDeliveryTimes}
          onChange={handleDeliveryTime}
        />
      </div>
      <button onClick={calcData}>Посчитать</button>
    </div>
  );
};

export default CalcView;
