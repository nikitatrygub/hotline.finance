import React, { Component, Fragment } from "react";
import {
  initializeDataAction,
  setCurrencyAction,
  setCurrentCreditSummAfterAction,
  setCurrentCreditSummAction,
  setDeliveryTimeAction,
  calcData
} from "../actions/rates";
import { connect } from "react-redux";
import CalcView from "../components/CalcView";
import rates from "../data.json";

class Calc extends Component {
  componentDidMount() {
    let { initializeData } = this.props;
    initializeData(rates);
  }
  render() {
    const {
      state,
      setCurrCurency,
      setCurrentCreditSummAfterAction,
      setCurrentCreditSummAction,
      calcData,
      setDeliveryTime
    } = this.props;
    return (
      <Fragment>
        {state.fetched && (
          <CalcView
            setCurrCurency={setCurrCurency}
            setCurrentCreditSummAfterAction={setCurrentCreditSummAfterAction}
            setCurrentCreditSummAction={setCurrentCreditSummAction}
            setDeliveryTime={setDeliveryTime}
            calcData={calcData}
            state={state}
          />
        )}
        {state.selectedRate &&
          state.selectedRate.map(rate => (
            <div key={rate.id}>{rate.rateUsd}</div>
          ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({ state: state });

const mapDispatchToState = dispatch => ({
  initializeData: data => dispatch(initializeDataAction(data)),
  setCurrCurency: currency => dispatch(setCurrencyAction(currency)),
  setCurrentCreditSummAfterAction: value =>
    dispatch(setCurrentCreditSummAfterAction(value)),
  setCurrentCreditSummAction: value =>
    dispatch(setCurrentCreditSummAction(value)),
  setDeliveryTime: value => dispatch(setDeliveryTimeAction(value)),
  calcData: () => dispatch(calcData())
});

export default connect(
  mapStateToProps,
  mapDispatchToState
)(Calc);
