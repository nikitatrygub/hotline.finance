import createBrowserHistory from "history/createBrowserHistory";
import queryString from "query-string";

const history = createBrowserHistory();

export const GET_RATES = "GET_RATES";
export const SET_CURCURENCY = "SET_CURCURENCY";
export const SET_CURRENT_CREDIT_SUMM_AFTER = "SET_CURRENT_CREDIT_SUMM_AFTER";
export const SET_DELIVERY_TIME = "SET_DELIVERY_TIME";
export const SET_CURRENT_CREDIT_SUMM = "SET_CURRENT_CREDIT_SUMM";

export function initializeDataAction(value) {
  return async (dispatch, getState) => {
    let rates = value;
    function getMinCreditSumm(currency) {
      let maxMinCreditsSumm = rates.map(rate => {
        if (currency === "usd") {
          return [rate.minCreditSummUsd];
        } else if (currency === "uah") {
          return [rate.minCreditSummUah];
        }
        return null;
      });

      let minCreditSumms = [].concat(...maxMinCreditsSumm);
      let minCreditSumm = minCreditSumms.reduce(
        (a, b) => Math.min(a, b),
        Infinity
      );

      return minCreditSumm;
    }

    function getMaxCreditSummUsd(currency) {
      let mapMaxCreditsSumm = rates.map(rate => {
        if (currency === "usd") {
          return [rate.maxCreditSummUsd];
        } else if (currency === "uah") {
          return [rate.maxCreditSummUah];
        }
        return null;
      });

      let maxCreditSumms = [].concat(...mapMaxCreditsSumm);
      let maxCreditSumm = maxCreditSumms.reduce((a, b) => Math.max(a, b));

      return maxCreditSumm;
    }

    function getAllDeliveryTimes() {
      let mapDeliveryTime = rates.map(rate => {
        return [rate.minDeliveryTime, rate.maxDeliveryTime];
      });

      let allDeliveryTimes = [].concat(...mapDeliveryTime);
      let minDeliveryTime = allDeliveryTimes.reduce(
        (a, b) => Math.min(a, b),
        Infinity
      );
      let maxDeliveryTime = allDeliveryTimes.reduce((a, b) => Math.max(a, b));
      let newArr = Array.from({
        length: maxDeliveryTime
      }).map((_, i) => {
        return {
          label: `${i + minDeliveryTime + " months"}`,
          value: i + minDeliveryTime
        };
      });

      return newArr;
    }

    let minCreditSummUah = getMinCreditSumm("uah");
    let minCreditSummUsd = getMinCreditSumm("usd");
    let maxCreditSummUah = getMaxCreditSummUsd("uah");
    let maxCreditSummUsd = getMaxCreditSummUsd("usd");
    let allDeliveryTimes = getAllDeliveryTimes();

    function getParamsFromUrl() {
      let { location } = history;
      if (location.search) {
        let urlParams = queryString.parse(location.search);
        let mapParamsToState = {
          currentCreditSumm: urlParams.currentSumm,
          currentCurrency: urlParams.currentCurrency,
          currentDeliveryTime: urlParams.currentDeliveryTime
          // currentMinCreditSumm: currentMinCreditSum,
          // currentMaxCreditSumm: currentMaxCreditSum
        };

        return { ...mapParamsToState };
      }
      return null;
    }

    let filteredRatesArr = filterRates(
      rates,
      minCreditSummUsd,
      "usd",
      allDeliveryTimes[0].value
    );

    let filteredArr = getFilteredArr(filteredRatesArr);

    dispatch({
      type: GET_RATES,
      rates,
      minCreditSummUah,
      minCreditSummUsd,
      maxCreditSummUah,
      maxCreditSummUsd,
      allDeliveryTimes,
      paramsFromUrl: getParamsFromUrl(),
      filteredArr
    });
  };
}

export function setCurrencyAction(currency) {
  return (dispatch, getState) => {
    let state = getState();

    function setCurrentCreditsSumm() {
      if (currency.value === "usd") {
        return {
          currentMinCreditSumm: state.minCreditSummUsd,
          currentMaxCreditSumm: state.maxCreditSummUsd
        };
      } else if (currency.value === "uah") {
        return {
          currentMinCreditSumm: state.minCreditSummUah,
          currentMaxCreditSumm: state.maxCreditSummUah
        };
      }
    }

    let filteredRatesArr = filterRates(
      state.rates,
      state.currentCreditSumm,
      currency.value,
      state.currentDeliveryTime.value
    );

    let filteredArr = getFilteredArr(filteredRatesArr);

    let currentCreditsSumm = setCurrentCreditsSumm();
    buildUrl(
      currentCreditsSumm.currentMinCreditSumm,
      currency.value,
      state.currentDeliveryTime.value
    );
    dispatch({
      type: SET_CURCURENCY,
      currency,
      currentCreditsSumm,
      filteredArr
    });
  };
}

export function setCurrentCreditSummAfterAction(value) {
  return (dispatch, getState) => {
    let state = getState();

    buildUrl(
      value,
      state.currentCurrency.value,
      state.currentDeliveryTime.value
    );
    let filteredRatesArr = filterRates(
      state.rates,
      value,
      state.currentCurrency.value,
      state.currentDeliveryTime.value
    );

    let filteredArr = getFilteredArr(filteredRatesArr);

    dispatch({
      type: SET_CURRENT_CREDIT_SUMM_AFTER,
      value,
      filteredArr
    });
  };
}

export function setCurrentCreditSummAction(value) {
  return dispatch => {
    dispatch({
      type: SET_CURRENT_CREDIT_SUMM,
      value
    });
  };
}

export function setDeliveryTimeAction(value) {
  return (dispatch, getState) => {
    let state = getState();
    buildUrl(state.currentCreditSumm, state.currentCurrency.value, value.value);
    let filteredRatesArr = filterRates(
      state.rates,
      state.currentCreditSumm,
      state.currentCurrency.value,
      value.value
    );
    let filteredArr = getFilteredArr(filteredRatesArr);
    dispatch({
      type: SET_DELIVERY_TIME,
      value,
      filteredArr
    });
  };
}

export function calcData() {
  return (dispatch, getState) => {
    let state = getState();
    let rate = state.currentRateUsd;
    let deliveryTime = Number(state.currentDeliveryTime.value);
    let coef = rate / 100 / 12;
    let currSumm = state.currentCreditSumm;

    function pow(x, n) {
      let result = x;
      for (let i = 1; i < n; i++) {
        result *= x;
      }
      return result;
    }

    let totalAmount =
      ((currSumm * coef * pow(1 + coef, deliveryTime)) /
        (pow(1 + coef, deliveryTime) - 1)) *
      deliveryTime;

    let mounthlyPayment = totalAmount / deliveryTime;
    let overpayment = totalAmount - currSumm;
    console.log(
      "Ставка:",
      rate + "%",
      "Полная сумма:",
      Math.round(totalAmount) + "USD",
      "Месячный платеж:",
      Math.round(mounthlyPayment) + "USD",
      "Переплата:",
      Math.round(overpayment) + "USD"
    );
  };
}

const buildUrl = (currSumm, currCurrency, currDeliveryTime) => {
  const str = `?currentSumm=${currSumm}&currentCurrency=${currCurrency}&currentDeliveryTime=${currDeliveryTime}`;
  history.replace({
    search: str
  });
};

const filterRates = (rates, currSumm, currCurrency, currDeliveryTime) => {
  let filterRatesByParams = arr =>
    arr.filter(rate => {
      if (currCurrency === "usd") {
        let rateItem =
          currSumm >= rate.minCreditSummUsd &&
          currSumm <= rate.maxCreditSummUsd &&
          currDeliveryTime >= rate.minDeliveryTime &&
          currDeliveryTime <= rate.maxDeliveryTime;
        return rateItem;
      } else {
        let rateItem =
          currSumm >= rate.minCreditSummUah &&
          currSumm <= rate.maxCreditSummUah &&
          currDeliveryTime >= rate.minDeliveryTime &&
          currDeliveryTime <= rate.maxDeliveryTime;
        return rateItem;
      }
    });
  let filteredRates = filterRatesByParams(rates);
  return filteredRates;
};

function getFilteredArr(filteredRatesArr) {
  if (filteredRatesArr.length > 0) {
    return filteredRatesArr;
  }
  return null;
}
