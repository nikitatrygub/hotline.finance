import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Calc from "./containers/Calc";

class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={Calc} />
      </Router>
    );
  }
}

export default App;
