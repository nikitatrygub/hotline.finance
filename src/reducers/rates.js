import { produce } from "immer";
import {
  GET_RATES,
  SET_CURCURENCY,
  SET_DELIVERY_TIME,
  SET_CURRENT_CREDIT_SUMM_AFTER,
  SET_CURRENT_CREDIT_SUMM
} from "../actions/rates";

const initialState = {
  rates: null,
  fetched: false,
  minCreditSummUsd: null,
  maxCreditSummUsd: null,
  minCreditSummUah: null,
  maxCreditSummUah: null,
  currentMinCreditSumm: null,
  currentMaxCreditSumm: null,
  allDeliveryTimes: null,
  currentDeliveryTime: null,
  currentCurrency: { value: "usd", label: "usd" },
  currentCreditSumm: null,
  selectedRate: null,
  currentRateUsd: null
};

function ratesReducer(state = initialState, action) {
  return produce(state, draft => {
    switch (action.type) {
      case GET_RATES:
        draft.fetched = true;
        draft.rates = action.rates;
        draft.minCreditSummUsd = action.minCreditSummUsd;
        draft.maxCreditSummUsd = action.maxCreditSummUsd;
        draft.minCreditSummUah = action.minCreditSummUah;
        draft.maxCreditSummUah = action.maxCreditSummUah;
        draft.allDeliveryTimes = action.allDeliveryTimes;
        draft.currentDeliveryTime = action.allDeliveryTimes[0];
        draft.currentMinCreditSumm = action.minCreditSummUsd;
        draft.currentMaxCreditSumm = action.maxCreditSummUsd;
        draft.currentCreditSumm = action.minCreditSummUsd;
        draft.selectedRate = action.filteredArr;
        draft.currentRateUsd = action.filteredArr
          ? action.filteredArr[0].rateUsd
          : null;
        if (action.paramsFromUrl) {
          draft.currentCreditSumm = Number(
            action.paramsFromUrl.currentCreditSumm
          );
          draft.currentCurrency = {
            value: `${action.paramsFromUrl.currentCurrency}`,
            label: `${action.paramsFromUrl.currentCurrency}`
          };
          draft.currentDeliveryTime = {
            value: `${action.paramsFromUrl.currentDeliveryTime}`,
            label: `${action.paramsFromUrl.currentDeliveryTime} months`
          };
          // draft.currentMinCreditSumm = action.paramsFromUrl.currentMinCreditSumm;
          // draft.currentMaxCreditSumm = action.paramsFromUrl.currentMaxCreditSumm;
        }
        break;
      case SET_CURCURENCY:
        draft.currentCurrency = action.currency;
        draft.currentMinCreditSumm =
          action.currentCreditsSumm.currentMinCreditSumm;
        draft.currentMaxCreditSumm =
          action.currentCreditsSumm.currentMaxCreditSumm;
        draft.currentCreditSumm =
          action.currentCreditsSumm.currentMinCreditSumm;
        draft.selectedRate = action.filteredArr;
        draft.currentRateUsd = action.filteredArr
          ? action.filteredArr[0].rateUsd
          : null;
        break;
      case SET_CURRENT_CREDIT_SUMM:
        draft.currentCreditSumm = action.value;
        break;
      case SET_CURRENT_CREDIT_SUMM_AFTER:
        draft.currentCreditSumm = action.value;
        draft.selectedRate = action.filteredArr;
        draft.currentRateUsd = action.filteredArr
          ? action.filteredArr[0].rateUsd
          : null;
        break;
      case SET_DELIVERY_TIME:
        draft.currentDeliveryTime = action.value;
        draft.selectedRate = action.filteredArr;
        draft.currentRateUsd = action.filteredArr
          ? action.filteredArr[0].rateUsd
          : null;
        break;
      default:
        return;
    }
  });
}

export default ratesReducer;
